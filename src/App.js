import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './page/HomePage/HomePage';
import LoginPage from './page/Login/LoginPage';
import Header from './Component/Header/Header';
import { Toaster } from 'react-hot-toast';
import DetailPage from './page/DetailPage/DetailPage';
import Footer from './Component/Footer/Footer';
import Layout from './layout/Layout';
import Spinner from './Component/Spinner/Spinner';

function App() {
  return (
    <div>
      <BrowserRouter>
        <Toaster position='top-right' reverseOrder={false} />
        {/* luôn luôn render ra thẻ header */}
        <Spinner />
        <Routes>
          <Route path='/' element={
            <Layout>
              <HomePage />
            </Layout>
          }>
          </Route>
          <Route path='/detail/:id' element={
            <Layout>
              <DetailPage />
            </Layout>
          }>
          </Route>
          {/* /: là tham số trong Route */}
          <Route path='/login' element={<LoginPage />}></Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
