import React from 'react'
import Header from '../Component/Header/Header'
import Footer from '../Component/Footer/Footer'

export default function Layout({ children }) {
    return (
        <div className="min-h-screen flex flex-col">
            <Header />
            <div className='flex-grow '>{children}</div>
            <Footer />
        </div>
    )
}
