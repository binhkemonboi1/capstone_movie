import React from 'react'
import { PacmanLoader } from 'react-spinners'

export default function Spinner() {
    return (
        <div className="h-screen w-screen bg-black fixed z-10 flex justify-center items-center">
            <PacmanLoader size={200} speedMultiplier={2} color="#36d7b7" />
        </div>
    )

}

// react Spinner
