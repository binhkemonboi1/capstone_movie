import React from 'react'
import { useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

export default function Header() {
    let navigate = useNavigate(); // dùng để điều hướng trang
    let { info } = useSelector((state) => state.userReducer);
    console.log("🚀  Header  info:", info);
    let handleLogout = () => {
        /**
         * 1. Logout đá ra trang chủ
         * 2. clear localStorage
         */
        window.location.href = "/"; //khác với navigate vì location chuyển trang có load
        localStorage.clear(); // xóa toàn bộ localStorage
    };
    let renderUserNav = () => {
        if (info) {
            return (
                <>
                    <span>{info.hoTen}</span>
                    <button onClick={handleLogout} className="btn-theme">Log out</button>
                </>
            );
        }
        return (
            <>
                <button onClick={() => {
                    navigate("/login");
                }}
                    className="btn-theme"
                >
                    Login
                </button>

                <button onClick={() => {
                    navigate("/login");
                }}
                    className="btn-theme"
                >
                    Register
                </button>
            </>
        )
    };

    return (
        <div className="shadow-lg shadow-black">
            <div className="container flex justify-between h-20">
                {/* container của tailwind chỉ đại diện cho 1 thuộc tính. Khác với BS */}
                <span className="text-5xl text-red-500 cursor-pointer"
                    onClick={() => {
                        navigate("/");
                    }}
                >
                    CyberFlix
                </span>
                <nav className="space-x-5">
                    {renderUserNav()}

                </nav>
            </div>
        </div>
    );
}
